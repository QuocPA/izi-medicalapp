import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:izi_medicalapp/screens/d1_doctor.dart';
import 'package:izi_medicalapp/screens/m1_medecines.dart';

final List<String> imgList = [
  'assets/images/doctor.jpg',
  'assets/images/nguyen-cuu.jpg',
  'assets/images/medicines.jpg',
];

final List<Widget> imageSliders = imgList
    .map((index) => Container(
          child: Container(
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                child: Stack(
                  children: <Widget>[
                    Image.asset(index, fit: BoxFit.cover, width: 1000.0),
                  ],
                )),
          ),
        ))
    .toList();

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: ListView(
        scrollDirection: Axis.vertical,
        children: [
          Container(
            height: 20,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20.0),
                    bottomRight: Radius.circular(20.0))),
          ),
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height / 4,
                padding: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20.0))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) => Doctor()));
                        },
                        child: Card(
                            child: Container(
                          width: MediaQuery.of(context).size.width / 4,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                height: 5,
                              ),
                              CircleAvatar(
                                radius: 40.0,
                                backgroundImage:
                                    AssetImage("assets/icons/doctor.jpg"),
                                backgroundColor: Colors.transparent,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "Bác sĩ",
                                style: TextStyle(
                                    fontWeight: FontWeight.w800,
                                    fontSize: 18.0),
                              )
                            ],
                          ),
                        ))),
                    InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) => Medicine()));
                        },
                        child: Card(
                            child: Container(
                          width: MediaQuery.of(context).size.width / 4,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                height: 5,
                              ),
                              CircleAvatar(
                                radius: 40.0,
                                backgroundImage:
                                    AssetImage("assets/icons/medicines.jpg"),
                                backgroundColor: Colors.transparent,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "Thuốc",
                                style: TextStyle(
                                    fontWeight: FontWeight.w800,
                                    fontSize: 18.0),
                              )
                            ],
                          ),
                        ))),
                    InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) => Medicine()));
                        },
                        child: Card(
                            child: Container(
                          width: MediaQuery.of(context).size.width / 4,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                height: 5,
                              ),
                              CircleAvatar(
                                radius: 40.0,
                                backgroundImage:
                                    AssetImage("assets/icons/diagnosis.png"),
                                backgroundColor: Colors.transparent,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "Chuẩn đoán",
                                style: TextStyle(
                                    fontWeight: FontWeight.w800,
                                    fontSize: 18.0),
                              )
                            ],
                          ),
                        ))),
                  ],
                )),
          ),
          // slider here
          Container(
              child: CarouselSlider(
            options: CarouselOptions(
              aspectRatio: 2.0,
              enlargeCenterPage: true,
              enableInfiniteScroll: false,
              initialPage: 2,
              autoPlay: true,
            ),
            items: imageSliders,
          )),
          SizedBox(
            height: 20.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: [
                  SizedBox(
                    width: 10.0,
                  ),
                  Text(
                    "Bác sĩ ở gần bạn",
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w800,
                        color: Colors.black),
                  ),
                ],
              ),
              FlatButton(
                child: Text(
                  "Xem thêm",
                  style: TextStyle(),
                ),
                onPressed: null,
              ),
            ],
          ),
          SizedBox(height: 10.0),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height / 4,
            child: ListView(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              children: [
                SizedBox(
                  width: 5.0,
                ),
                InkWell(
                    onTap: null,
                    child: Card(
                        child: Container(
                      width: MediaQuery.of(context).size.width / 3,
                      child: Row(
                        children: [
                          SizedBox(
                            width: 5.0,
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 5,
                                ),
                                CircleAvatar(
                                  radius: 40.0,
                                  backgroundImage:
                                      AssetImage("assets/icons/doctor.jpg"),
                                  backgroundColor: Colors.transparent,
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  "BS. Trần Văn Nam",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w800,
                                      fontSize: 14.0),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  "Ung Bứu",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 10.0),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Icon(
                                      Icons.star_half,
                                      color: Colors.yellow,
                                    ),
                                    Text("4.5")
                                  ],
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ))),
                InkWell(
                    onTap: null,
                    child: Card(
                        child: Container(
                      width: MediaQuery.of(context).size.width / 3,
                      child: Row(
                        children: [
                          SizedBox(
                            width: 5.0,
                          ),
                          Expanded(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 5,
                              ),
                              CircleAvatar(
                                radius: 40.0,
                                backgroundImage:
                                    AssetImage("assets/icons/doctor.jpg"),
                                backgroundColor: Colors.transparent,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "BS. Nguyễn Phi Hùng",
                                style: TextStyle(
                                    fontWeight: FontWeight.w800,
                                    fontSize: 14.0),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "Nội Khoa",
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 10.0),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Row(
                                children: [
                                  Icon(
                                    Icons.star_half,
                                    color: Colors.yellow,
                                  ),
                                  Text("4.5")
                                ],
                              )
                            ],
                          ))
                        ],
                      ),
                    ))),
                InkWell(
                    onTap: null,
                    child: Card(
                        child: Container(
                      width: MediaQuery.of(context).size.width / 3,
                      child: Row(
                        children: [
                          SizedBox(
                            width: 5.0,
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 5,
                                ),
                                CircleAvatar(
                                  radius: 40.0,
                                  backgroundImage:
                                      AssetImage("assets/icons/doctor.jpg"),
                                  backgroundColor: Colors.transparent,
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  "BS. Nguyễn Quang Đạt",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w800,
                                      fontSize: 14.0),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  "Mắt",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 10.0),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Icon(
                                      Icons.star_half,
                                      color: Colors.yellow,
                                    ),
                                    Text("4.5")
                                  ],
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ))),
                InkWell(
                    onTap: null,
                    child: Card(
                        child: Container(
                      width: MediaQuery.of(context).size.width / 3,
                      child: Row(
                        children: [
                          SizedBox(
                            width: 5.0,
                          ),
                          Expanded(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 5,
                              ),
                              CircleAvatar(
                                radius: 40.0,
                                backgroundImage:
                                    AssetImage("assets/icons/doctor.jpg"),
                                backgroundColor: Colors.transparent,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "BS. Nguyễn Ngọc Anh",
                                style: TextStyle(
                                    fontWeight: FontWeight.w800,
                                    fontSize: 14.0),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "Phục Hồi Chức Năng",
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 10.0),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Row(
                                children: [
                                  Icon(
                                    Icons.star_half,
                                    color: Colors.yellow,
                                  ),
                                  Text("4.5")
                                ],
                              )
                            ],
                          ))
                        ],
                      ),
                    ))),
              ],
            ),
          ),
          SizedBox(
            height: 20.0,
          )
        ],
      ),
    );
  }
}
