import 'package:flutter/material.dart';
import 'package:search_widget/search_widget.dart';

class Doctor extends StatefulWidget {
  @override
  _DoctorState createState() => _DoctorState();
}

class _DoctorState extends State<Doctor> {
  TextEditingController _controllerFind = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text("Danh Sách Bác Sĩ"),
        elevation: 0.0,
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        children: [
          Container(
            height: 20,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20.0),
                    bottomRight: Radius.circular(20.0))),
          ),
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              child: TextField(
                controller: _controllerFind,
                style: TextStyle(fontSize: 16),
                decoration: InputDecoration(
                  enabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                  suffixIcon: Icon(
                    Icons.search,
                    color: Colors.blue,
                  ),
                  border: InputBorder.none,
                  hintText: "Search here...",
                  contentPadding: const EdgeInsets.only(
                    left: 16,
                    right: 20,
                    top: 14,
                    bottom: 14,
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding:
                EdgeInsets.only(left: 25.0, top: 5.0, right: 25.0, bottom: 5.0),
            child: InkWell(
              onTap: null,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25.0),
                ),
                child: Container(
                  height: MediaQuery.of(context).size.height / 6,
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircleAvatar(
                              radius: 30.0,
                              backgroundImage:
                                  AssetImage("assets/icons/doc1.png"),
                              backgroundColor: Colors.transparent,
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              "Bận hôm nay",
                              style: TextStyle(color: Colors.red),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "BS. Trần Văn Nam",
                              style: TextStyle(fontWeight: FontWeight.w800),
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            Text(
                                "Chuyên Khoa Ung Bứu với hơn 10 năm kinh nghiệm",
                                style: TextStyle(fontWeight: FontWeight.w200)),
                            SizedBox(
                              height: 5.0,
                            ),
                            Text("08:00AM - 17:00PM",
                                style: TextStyle(color: Colors.blue))
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding:
                EdgeInsets.only(left: 25.0, top: 5.0, right: 25.0, bottom: 5.0),
            child: InkWell(
              onTap: null,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25.0),
                ),
                child: Container(
                  height: MediaQuery.of(context).size.height / 6,
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircleAvatar(
                              radius: 30.0,
                              backgroundImage:
                                  AssetImage("assets/icons/doc2.jpg"),
                              backgroundColor: Colors.transparent,
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              "Đang hoạt động",
                              style: TextStyle(color: Colors.green[300]),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "BS. Nguyễn Phi Hùng",
                              style: TextStyle(fontWeight: FontWeight.w800),
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            Text(
                                "Chuyên Khoa Ung Bứu với hơn 10 năm kinh nghiệm",
                                style: TextStyle(fontWeight: FontWeight.w200)),
                            SizedBox(
                              height: 5.0,
                            ),
                            Text("08:00AM - 17:00PM",
                                style: TextStyle(color: Colors.blue))
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding:
                EdgeInsets.only(left: 25.0, top: 5.0, right: 25.0, bottom: 5.0),
            child: InkWell(
              onTap: null,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25.0),
                ),
                child: Container(
                  height: MediaQuery.of(context).size.height / 6,
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircleAvatar(
                              radius: 30.0,
                              backgroundImage:
                                  AssetImage("assets/icons/doc3.png"),
                              backgroundColor: Colors.transparent,
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              "Bận hôm nay",
                              style: TextStyle(color: Colors.red),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "BS. Nguyên Quang Đạt",
                              style: TextStyle(fontWeight: FontWeight.w800),
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            Text(
                                "Chuyên Khoa Ung Bứu với hơn 10 năm kinh nghiệm",
                                style: TextStyle(fontWeight: FontWeight.w200)),
                            SizedBox(
                              height: 5.0,
                            ),
                            Text("08:00AM - 17:00PM",
                                style: TextStyle(color: Colors.blue))
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding:
                EdgeInsets.only(left: 25.0, top: 5.0, right: 25.0, bottom: 5.0),
            child: InkWell(
              onTap: null,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25.0),
                ),
                child: Container(
                  height: MediaQuery.of(context).size.height / 6,
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircleAvatar(
                              radius: 30.0,
                              backgroundImage:
                                  AssetImage("assets/icons/doctor.jpg"),
                              backgroundColor: Colors.transparent,
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              "Bận hôm nay",
                              style: TextStyle(color: Colors.red),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "BS. Nguyễn Ngọc Anh",
                              style: TextStyle(fontWeight: FontWeight.w800),
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            Text(
                                "Chuyên Khoa Ung Bứu với hơn 10 năm kinh nghiệm",
                                style: TextStyle(fontWeight: FontWeight.w200)),
                            SizedBox(
                              height: 5.0,
                            ),
                            Text("08:00AM - 17:00PM",
                                style: TextStyle(color: Colors.blue))
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
