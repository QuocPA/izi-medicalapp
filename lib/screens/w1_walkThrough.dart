import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:izi_medicalapp/screens/l1_login.dart';

class Walkthrough extends StatefulWidget {
  @override
  _WalkthroughState createState() => _WalkthroughState();
}

class _WalkthroughState extends State<Walkthrough> {
  List pageInfos = [
    {
      "title": "Mang bác sĩ đến với bạn",
      "body": "Những bác sĩ giỏi hàng đầu cả nước ở ngay gần bạn.",
      "img": "assets/images/doctor1.png",
    },
    {
      "title": "Đặt lịch hẹn dễ dàng",
      "body": "Đặt những lịch hẹn với bác sĩ hiện có.",
      "img": "assets/images/doctor2.png",
    },
    {
      "title": "Chuẩn đoán dễ dàng",
      "body":
          "Kho tư liệu lớn giúp bạn dễ dàng trong việc kiểm tra sức khỏe của mình.",
      "img": "assets/images/doctor3.png",
    },
  ];
  @override
  Widget build(BuildContext context) {
    List<PageViewModel> pages = [
      for (int i = 0; i < pageInfos.length; i++) _buildPageModel(pageInfos[i])
    ];

    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        body: Padding(
          padding: EdgeInsets.only(left: 10, top: 30, right: 10, bottom: 10),
          child: IntroductionScreen(
            pages: pages,
            onDone: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) {
                    return Login();
                  },
                ),
              );
            },
            onSkip: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) {
                    return Login();
                  },
                ),
              );
            },
            showSkipButton: true,
            skip: Text("Bỏ qua"),
            next: Text(
              "Tiếp",
              style: TextStyle(
                fontWeight: FontWeight.w800,
                color: Theme.of(context).accentColor,
              ),
            ),
            done: Text(
              "Hoàn thành",
              style: TextStyle(
                fontWeight: FontWeight.w800,
                color: Theme.of(context).accentColor,
              ),
            ),
          ),
        ),
      ),
    );
  }

  _buildPageModel(Map item) {
    return PageViewModel(
      title: item['title'],
      body: item['body'],
      image: Image.asset(
        item['img'],
        height: 150.0,
      ),
      decoration: PageDecoration(
        titleTextStyle: TextStyle(
          fontSize: 28.0,
          fontWeight: FontWeight.w600,
          color: Theme.of(context).accentColor,
        ),
        bodyTextStyle: TextStyle(fontSize: 15.0),
//        dotsDecorator: DotsDecorator(
//          activeColor: Theme.of(context).accentColor,
//          activeSize: Size.fromRadius(8),
//        ),
        pageColor: Theme.of(context).primaryColor,
      ),
    );
  }
}
