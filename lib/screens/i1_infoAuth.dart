import 'package:flutter/material.dart';
import 'package:izi_medicalapp/screens/m1_mainScreen.dart';
import 'package:izi_medicalapp/widgets/c1_customDialog.dart';
import 'package:izi_medicalapp/widgets/t1_timelLine.dart';

class InfoAuth extends StatefulWidget {
  @override
  _InfoAuthState createState() => _InfoAuthState();
}

class _InfoAuthState extends State<InfoAuth> {
  TextEditingController _controllerName = TextEditingController();
  TextEditingController _controllerPhone = TextEditingController();
  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerAge = TextEditingController();
  TextEditingController _controllerAddress = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text("Xác nhận thông tin"),
        elevation: 0.0,
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        children: [
          Container(
            height: 20,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20.0),
                    bottomRight: Radius.circular(20.0))),
          ),
          SizedBox(
            height: 10.0,
          ),
          ScreenProgress(ticks: 3),
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20.0),
                    bottomRight: Radius.circular(20.0),
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0),
                  )),
              height: MediaQuery.of(context).size.height / 2,
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                        left: 10.0, top: 10.0, right: 10.0, bottom: 5.0),
                    child: TextField(
                      keyboardType: TextInputType.text,
                      autocorrect: false,
                      controller: _controllerName,
                      decoration: InputDecoration(
                          hintText: "Họ Tên", border: OutlineInputBorder()),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 10.0, top: 10.0, right: 10.0, bottom: 5.0),
                    child: TextField(
                      keyboardType: TextInputType.text,
                      autocorrect: false,
                      controller: _controllerPhone,
                      decoration: InputDecoration(
                          hintText: "Số Điện Thoại",
                          border: OutlineInputBorder()),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 10.0, top: 10.0, right: 10.0, bottom: 5.0),
                    child: TextField(
                      keyboardType: TextInputType.text,
                      autocorrect: false,
                      controller: _controllerEmail,
                      decoration: InputDecoration(
                          hintText: "Email", border: OutlineInputBorder()),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 10.0, top: 10.0, right: 10.0, bottom: 5.0),
                    child: TextField(
                      keyboardType: TextInputType.text,
                      autocorrect: false,
                      controller: _controllerAge,
                      decoration: InputDecoration(
                          hintText: "Tuổi", border: OutlineInputBorder()),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 10.0, top: 10.0, right: 10.0, bottom: 5.0),
                    child: TextField(
                      keyboardType: TextInputType.text,
                      autocorrect: false,
                      controller: _controllerAddress,
                      decoration: InputDecoration(
                          hintText: "Địa Chỉ", border: OutlineInputBorder()),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
      bottomSheet: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(10, 5, 5, 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Tổng tiền",
                    style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  Text(
                    "120,000 VNĐ",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w900,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  Text(
                    "Đã bao gồm VAT",
                    style: TextStyle(
                      fontSize: 11,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(5, 5, 10, 5),
              width: 150.0,
              height: 50.0,
              child: FlatButton(
                color: Theme.of(context).accentColor,
                child: Text(
                  "Đặt Lịch Hẹn".toUpperCase(),
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) => CustomDialog(
                            title: "DoctorCare",
                            description: "Đặt lịch thành công !",
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      MainScreen()));
                            },
                          ));
                },
              ),
            ),
          ],
        ),
        height: 70,
      ),
    );
  }
}
