import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:percent_indicator/percent_indicator.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 3,
              decoration: BoxDecoration(
                  color: Color(0xFF7986CB),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20.0),
                    bottomRight: Radius.circular(20.0),
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0),
                  )),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Icon(
                        Icons.person_outline,
                        color: Colors.white,
                      ),
                      CircleAvatar(
                        radius: 30.0,
                        backgroundImage: AssetImage("assets/icons/user_.png"),
                        backgroundColor: Colors.transparent,
                      ),
                      Icon(
                        Icons.settings,
                        color: Colors.white,
                      )
                    ],
                  ),
                  Text(
                    "Pham Anh Quoc",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.w800),
                  ),
                  Text(
                    "+84 79960 3460",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.w800),
                  ),
                  new CircularPercentIndicator(
                    radius: 100.0,
                    lineWidth: 10.0,
                    percent: 0.8,
                    center: new Icon(
                      Icons.person_pin,
                      size: 50.0,
                      color: Colors.black,
                    ),
                    backgroundColor: Colors.blue[800],
                    progressColor: Colors.white,
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.4,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20.0),
                    bottomRight: Radius.circular(20.0),
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0),
                  )),
              child: ListView(
                children: [
                  ListTile(
                    leading: Icon(FontAwesomeIcons.fileContract,
                        color: Color(0xFF7986CB)),
                    title: Text(
                      "Bác Sĩ Của Tôi",
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w800),
                    ),
                    trailing: Icon(
                      Icons.navigate_next,
                      color: Color(0xFF7986CB),
                    ),
                    onTap: null,
                  ),
                  Divider(
                    color: Colors.grey[300],
                    indent: 20,
                    endIndent: 20,
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.calendar_today,
                      color: Color(0xFF7986CB),
                    ),
                    title: Text("Lịch Hẹn",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w800)),
                    trailing: Icon(
                      Icons.navigate_next,
                      color: Color(0xFF7986CB),
                    ),
                  ),
                  Divider(
                    color: Colors.grey[300],
                    indent: 20,
                    endIndent: 20,
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.card_giftcard,
                      color: Color(0xFF7986CB),
                    ),
                    title: Text("Phiếu Quà Tặng",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w800)),
                    trailing: Icon(
                      Icons.navigate_next,
                      color: Color(0xFF7986CB),
                    ),
                  ),
                  Divider(
                    color: Colors.grey[300],
                    indent: 20,
                    endIndent: 20,
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.payment,
                      color: Color(0xFF7986CB),
                    ),
                    title: Text("Thanh Toán",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w800)),
                    trailing: Icon(
                      Icons.navigate_next,
                      color: Color(0xFF7986CB),
                    ),
                  ),
                  Divider(
                    color: Colors.grey[300],
                    indent: 20,
                    endIndent: 20,
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.payment,
                      color: Color(0xFF7986CB),
                    ),
                    title: Text("Thanh Toán",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w800)),
                    trailing: Icon(
                      Icons.navigate_next,
                      color: Color(0xFF7986CB),
                    ),
                  ),
                  Divider(
                    color: Colors.grey[300],
                    indent: 20,
                    endIndent: 20,
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.local_offer,
                      color: Color(0xFF7986CB),
                    ),
                    title: Text("Gợi Ý Cho Bạn",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w800)),
                    trailing: Icon(
                      Icons.navigate_next,
                      color: Color(0xFF7986CB),
                    ),
                  ),
                  Divider(
                    color: Colors.grey[300],
                    indent: 20,
                    endIndent: 20,
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.exit_to_app,
                      color: Color(0xFF7986CB),
                    ),
                    title: Text("Đăng Xuất",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w800)),
                    trailing: Icon(
                      Icons.navigate_next,
                      color: Color(0xFF7986CB),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
