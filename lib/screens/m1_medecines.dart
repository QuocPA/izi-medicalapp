import 'package:flutter/material.dart';
import 'package:izi_medicalapp/screens/c1_checkout.dart';

class Medicine extends StatefulWidget {
  @override
  _MedicineState createState() => _MedicineState();
}

class _MedicineState extends State<Medicine> {
  TextEditingController _controllerFind = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text("Danh Sách Thuốc"),
        elevation: 0.0,
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        children: [
          Container(
            height: 20,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20.0),
                    bottomRight: Radius.circular(20.0))),
          ),
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              child: TextField(
                controller: _controllerFind,
                style: TextStyle(fontSize: 16),
                decoration: InputDecoration(
                  enabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                  suffixIcon: Icon(
                    Icons.search,
                    color: Colors.blue,
                  ),
                  border: InputBorder.none,
                  hintText: "Search here...",
                  contentPadding: const EdgeInsets.only(
                    left: 16,
                    right: 20,
                    top: 14,
                    bottom: 14,
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Text(
              "Danh sách thuốc:",
              style: TextStyle(fontWeight: FontWeight.w800, fontSize: 18.0),
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.5,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20.0),
                  bottomRight: Radius.circular(20.0),
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0),
                )),
            child: ListView(
              children: [
                SizedBox(
                  height: 10.0,
                ),
                ListTile(
                  onTap: null,
                  leading: CircleAvatar(
                    radius: 30.0,
                    backgroundImage: AssetImage("assets/icons/medicines.jpg"),
                    backgroundColor: Colors.transparent,
                  ),
                  title: Text("Paracetamol"),
                  subtitle: Text("20.000 VNĐ"),
                  trailing: Icon(
                    Icons.add_circle_outline,
                    color: Colors.blue[800],
                  ),
                ),
                const Divider(
                  color: Colors.black26,
                  indent: 10,
                  endIndent: 10,
                ),
                SizedBox(
                  height: 10.0,
                ),
                ListTile(
                  onTap: null,
                  leading: CircleAvatar(
                    radius: 30.0,
                    backgroundImage: AssetImage("assets/icons/medicines.jpg"),
                    backgroundColor: Colors.transparent,
                  ),
                  title: Text("Paracetamol"),
                  subtitle: Text("20.000 VNĐ"),
                  trailing: Icon(
                    Icons.add_circle_outline,
                    color: Colors.blue[800],
                  ),
                ),
                const Divider(
                  color: Colors.black26,
                  indent: 10,
                  endIndent: 10,
                ),
                SizedBox(
                  height: 10.0,
                ),
                ListTile(
                  onTap: null,
                  leading: CircleAvatar(
                    radius: 30.0,
                    backgroundImage: AssetImage("assets/icons/medicines.jpg"),
                    backgroundColor: Colors.transparent,
                  ),
                  title: Text("Paracetamol"),
                  subtitle: Text("20.000 VNĐ"),
                  trailing: Icon(
                    Icons.add_circle_outline,
                    color: Colors.blue[800],
                  ),
                ),
                const Divider(
                  color: Colors.black26,
                  indent: 10,
                  endIndent: 10,
                ),
                SizedBox(
                  height: 10.0,
                ),
                ListTile(
                  onTap: null,
                  leading: CircleAvatar(
                    radius: 30.0,
                    backgroundImage: AssetImage("assets/icons/medicines.jpg"),
                    backgroundColor: Colors.transparent,
                  ),
                  title: Text("Paracetamol"),
                  subtitle: Text("20.000 VNĐ"),
                  trailing: Icon(
                    Icons.add_circle_outline,
                    color: Colors.blue[800],
                  ),
                ),
                const Divider(
                  color: Colors.black26,
                  indent: 10,
                  endIndent: 10,
                ),
                SizedBox(
                  height: 10.0,
                ),
                ListTile(
                  onTap: null,
                  leading: CircleAvatar(
                    radius: 30.0,
                    backgroundImage: AssetImage("assets/icons/medicines.jpg"),
                    backgroundColor: Colors.transparent,
                  ),
                  title: Text("Paracetamol"),
                  subtitle: Text("20.000 VNĐ"),
                  trailing: Icon(
                    Icons.add_circle_outline,
                    color: Colors.blue[800],
                  ),
                ),
                const Divider(
                  color: Colors.black26,
                  indent: 10,
                  endIndent: 10,
                ),
              ],
            ),
          ),
        ],
      ),
      bottomSheet: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(10, 5, 5, 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Tổng tiền",
                    style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  Text(
                    "120,000 VNĐ",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w900,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  Text(
                    "Đã bao gồm VAT",
                    style: TextStyle(
                      fontSize: 11,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(5, 5, 10, 5),
              width: 150.0,
              height: 50.0,
              child: FlatButton(
                color: Theme.of(context).accentColor,
                child: Text(
                  "Đặt Hàng".toUpperCase(),
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => Checkout()));
                },
              ),
            ),
          ],
        ),
        height: 70,
      ),
    );
  }
}
